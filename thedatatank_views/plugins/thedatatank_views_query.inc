<?php

class thedatatank_views_query extends views_plugin_query {
	
	function init($base_table, $base_field, $options) {
		parent::init($base_table, $base_field, $options);
	}
	
  function add_field($table, $field, $alias = '', $params = array()) {
    // We check for this specifically because it gets a special alias.
    if ($table == $this->base_table && $field == $this->base_field && empty($alias)) {
      $alias = $this->base_field;
    }

    if ($table && empty($this->table_queue[$table])) {
      $this->ensure_table($table);
    }

    if (!$alias && $table) {
      $alias = $table . '_' . $field;
    }

    // Make sure an alias is assigned
    $alias = $alias ? $alias : $field;

    // PostgreSQL truncates aliases to 63 characters: http://drupal.org/node/571548

    // We limit the length of the original alias up to 60 characters
    // to get a unique alias later if its have duplicates
    $alias = substr($alias, 0, 60);

    // Create a field info array.
    $field_info = array(
      'field' => $field,
      'table' => $table,
      'alias' => $alias,
    ) + $params;

    // Test to see if the field is actually the same or not. Due to
    // differing parameters changing the aggregation function, we need
    // to do some automatic alias collision detection:
    $base = $alias;
    $counter = 0;
    while (!empty($this->fields[$alias]) && $this->fields[$alias] != $field_info) {
      $field_info['alias'] = $alias = $base . '_' . ++$counter;
    }

    if (empty($this->fields[$alias])) {
      $this->fields[$alias] = $field_info;
    }

    return $alias;
  }
  
  /**
   * Build the query object.
   */
  public function build(&$view) {
  // broken due to upgrade to views-6.x-3.x-dev // $this->init_pager($view);
    $view->init_pager();

    // Let the pager modify the query to add limits.
    $this->pager->query();

    if ($this->pager->use_pager()) {
      $this->pager->set_current_page($view->current_page);
    }
  }
	
	/**
	 * Ensure table
	 */
	function ensure_table() {
		//TODO ensure the correct table, this makes it easier to use the get_value in a correct way and also to get better structured code
		return 'thedatatankresources';
	}
	
	/**
	 * Execute the query
	 */
	function execute(&$view) {
		//Start query
		$start = microtime(TRUE);
		
		//Data
		switch ($this->base_table) {
			case 'thedatatankresources':
        $objects = thedatatank_get_resources();
        $result = array();
        if (count($objects) > 0) {
        	foreach ($objects as $resource => $packages) {
        		$result[]['resource_name'] = $resource;
        	} 
        }
        
				break;
			case 'thedatatankpackages':
				$objects = thedatatank_get_resources();
        $result = array();

        foreach ($objects as $resource => $packages) {
        	if (isset($this->filters) && in_array($resource, $this->filters['resource_name'])) {
            foreach ($packages as $package => $info) {
          	 $row = array(
          	   'thedatatankresources_resource_name' => $resource,
          	   'thedatatankresources_package_name' => $package,
          	 );
          	
          	 $row = (object)$row;
          	 $result[] = $row;
            }
        	} elseif (!isset($this->filters)) {
        		foreach ($packages as $package => $info) {
             $row = array(
               'thedatatankresources_resource_name' => $resource,
               'thedatatankresources_package_name' => $package,
             );
            
             $row = (object)$row;
             $result[] = $row;
            }
        	}
        }
        
				break;
				
			default:
				//Dynamic sources will be handled here
				$base_table = explode('_', $this->base_table);
				$resource = $base_table[1];
				$package = $base_table[2];

				$uri = thedatatank_build_request($resource, $package);
				$data = thedatatank_perform_request($uri);
				$data = json_decode($data);
				$data = $data->$package;
				
				$result = array();
				
				foreach ($data as $object) {
				  $row = array();
				  $object = (array)$object;
				  
				  $filtered = FALSE;
				  
				  //Filter this row
				  if (isset($this->filters)) {
				  	foreach ($this->filters as $key => $value) {
				  		$filtered = FALSE;
				  		
				  		$filterfield = str_replace(array('field_', '_available'), '', $key);
				  		
				  		if (!is_array($value)) {
				  		  if ($object[$filterfield] != $value) {
				  			 $filtered = TRUE;
				  		  }
				  		} else {
				  			if (!in_array($object[$filterfield], $value)) $filtered = TRUE;
				  		}
				  	}
				  }
          
				  if (!$filtered) {
				    foreach ($object as $key => $value) {
				  	 $row['field_' . $key] = $value;
				    }
				    
				    $result[] = $row;
				  }
				}

				break;
		}
		
		//Paging
		if ($this->pager->use_pager()) {
			$this->pager->total_items = count($result);
			$current_page = $this->pager->current_page;
			$limit = $this->limit;
			
			$startkey = $current_page * $limit;
			
			$result = array_slice($result, $startkey, $limit);
		}

		if (count($result) > 0) {
			$view->result = $result;
      $this->pager->update_page_info();
      $view->pager['current_page'] = $this->pager->current_page;
		}
		
		//End query
		$view->execute_time = microtime() - $start;
	}
	
  /**
   * Construct the query_params array for cnapi_get_events()
   * @param unknown_type $where
   */
  function _get_query_params($where) {
    $params = array();

    if (is_array($where)) {
      foreach ($where as $group => $group_where) {
        if (is_array($group_where['conditions']) && !empty($group_where['conditions'])) {
          foreach ($group_where['conditions'] as $condition) {
            $params = array_merge($params, $condition);
          }
        }
      }
    }

    return $params;
  }

  function add_where($group, $clause, $value, $operator) {
    $clause = explode('.', $clause);
    $clause = $clause[1];
    
    $this->filters[$clause] = $value;
  }

  function add_orderby($sort = NULL, $direction = NULL) {
    if (!is_null($sort) && !is_null($direction)) {
      $this->orderby = $sort . ' ' . $direction;
    }
  }
}