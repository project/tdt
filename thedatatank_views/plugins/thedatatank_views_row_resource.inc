<?php

/**
 * Row plugin for The DataTank Resource
 */
class thedatatank_views_row_resource extends views_plugin_row {

  function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);
    $this->base_table = $view->base_table;
    $this->base_field = 'thedatatank_field';
  }

  /**
   * Render each $row.
   */
  function render($row) {
  	return $row['resource_name'];
  }
}