<?php

/**
 * @file
 * Views integration for The DataTank
 */

/**
 * Implements hook_views_data().
 */
function thedatatank_views_data() {
	/** RESOURCES **/
	$data['thedatatankresources']['table']['group'] = t('The DataTank Resources');
	$data['thedatatankresources']['table']['base'] = array(
	 'field' => 'resource_name',
	 'title' => t('The DataTank Resources'),
	 'help' => t('The DataTank Resources'),
	 'query class' => 'thedatatank_views_query',
	);
	
	/** PACKAGES **/
	$data['thedatatankpackages']['table']['group'] = t('The DataTank Packages');
	$data['thedatatankpackages']['table']['base'] = array(
	 'field' => 'package_name',
	 'title' => t('The DataTank Packages'),
	 'help' => t('The DataTank Packages'),
	 'query class' => 'thedatatank_views_query',
	);
	
	/** FIELDS **/
	$data['thedatatankpackages']['package_name'] = array(
	 'title' => t('Package name'),
	 'help' => t('The name of the package in The DataTank.'),
	 'field' => array(
	   'handler' => 'thedatadank_views_handler_field_thedatatank',
	   'click sortable' => TRUE,
	 ),
	);
	
	$data['thedatatankpackages']['resource_name'] = array(
	 'title' => t('Resource name'),
	 'help' => t('The name of the resouce the package belongs to in The DataTank.'),
	 'field' => array(
	   'handler' => 'thedatadank_views_handler_field_thedatatank',
     'click sortable' => TRUE,
	 ),
	 'filter' => array(
	   'help' => t('Filter the result list on the resource in The DataTank.'),
	   'handler' => 'thedatatank_views_handler_filter_resource_name',
	 ),
	);
	
	$data['thedatatankpackages']['documentation'] = array(
	 'title' => t('Documentation'),
	 'help' => t('Documentation about this package provided by The DataTank'),
	 'field' => array(
	   'handler' => 'thedatadank_views_handler_field_thedatatank',
	   'click sortable' => FALSE,
	 ),
	);
	
	//Formatter fields
	$allowed_formatters = array_filter(variable_get('thedatatank_export_formatters', array()));
	if (empty($allowed_formatters)) $allowed_formatters = thedatatank_get_formatters();

	foreach ($allowed_formatters as $formatter) {
    $data['thedatatankpackages']['resource_export_link_' . strtolower($formatter)] = array(
      'title' => t('Link to @format', array('@format' => strtoupper($formatter))),
      'help' => t('Show the data of this formatter in @format format', array('@format' => strtoupper($formatter))),
      'field' => array(
        'handler' => 'thedatadank_views_handler_field_thedatatank',
        'click sortable' => FALSE,
      ),
    );	
  }
  
  /**
   * DYNAMIC RESOURCES AND FIELDS
   * We would like to use Views UI to search in The DataTank data. This would be great if we can fix it :)
   */
  $resources = thedatatank_get_resources();
  
  foreach($resources as $resource => $packages) {
  	foreach ($packages as $package => $info) {
  		//Get the data
  		$uri = thedatatank_build_request($resource, $package);
      $tankdata = thedatatank_perform_request($uri);
      $tankdata = json_decode($tankdata);
      
      //If there is data, make it possible to search on this
      if (!empty($tankdata)) {
	  		//Create a content selection type for the package
	  		$documentation = (isset($info->documentation) ? $info->documentation : $info->doc);
	  		$data['thedatatankresources_' . $resource . '_' . $package]['table']['group'] = t('The DataTank: @resource @package', array('@resource' => $resource, '@package' => $package));
	  		$data['thedatatankresources_' . $resource . '_' . $package]['table']['base'] = array(
	  		  'field' => 'field_rownumber',
	  		  'title' => t('The DataTank: @resource @package', array('@resource' => $resource, '@package' => $package)),
	  		  'help' => t($documentation),
	  		  'query class' => 'thedatatank_views_query',
	  		);
	  		
	  		//Get the fields :) This will be funny I think :)
	      $tankdata = $tankdata->{$package};
        $fields = array_keys((array)$tankdata[0]);

        foreach ($fields as $field) {
        	//Regular field and text filter
        	$data['thedatatankresources_' . $resource . '_' . $package]['field_' . $field] = array(
        	 'title' => t(ucfirst($field)),
        	 'help' => t('Data from @field field', array('@field' => $field)),
        	 'field' => array(
        	   'handler' => 'thedatatank_views_handler_dynamic_field',
        	   'click sortable' => FALSE,
        	 ),
        	 'filter' => array(
        	   'help' => t('Filter the results on @field', array('@field' => $field)),
        	   'handler' => 'views_handler_filter_string',
        	 ),
        	);
        	
        	//Available values filter
          $data['thedatatankresources_' . $resource . '_' . $package]['field_' . $field . '_available'] = array(
            'title' => t('Available data from @field', array('@field' => $field)),
            'help' => t('Available data from @field field', array('@field' => $field)),
            'filter' => array(
              'help' => t('Filter the results on available data in @field field.', array('@field' => $field)),
              'handler' => 'thedatatank_views_handler_filter_available_data',
            ),
          );
        }
      }  		
  	}
  }
  	
	return $data;
}

/**
 * Implements hook_views_plugins().
 */
function thedatatank_views_views_plugins() {
	$path = drupal_get_path('module', 'thedatatank_views');

	return array(
	 'module' => 'thedatatank_views',
	 'query' => array(
	   'thedatatank_views_query' => array(
	     'uses fields' => FALSE,
	     'path' => $path . '/plugins',
	     'title' => t('The DataTank Query'),
	     'help' => t('Query The DataTank from Views.'),
	     'handler' => 'thedatatank_views_query',
	     'parent' => 'views_query',
	   ),
	 ),
	 'row' => array(
	   'thedatatank_resource' => array(
	     'title' => t('The DataTank resource'),
	     'handler' => 'thedatatank_views_row_resource',
	     'path' => $path . '/plugins',
	     'base' => array('thedatatankresources'),
	     'uses options' => FALSE,
	     'type' => 'normal',
	   ),
	 ),
	);
}