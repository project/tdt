<?php

/**
 * Filter by available data.
 *
 * @ingroup views_filter_handlers
 */
class thedatatank_views_handler_filter_available_data extends views_handler_filter_in_operator {
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    //Get package and resource
    $table = explode('_', $this->table);
    $resource = $table[1];
    $package = $table[2];
    
    //Get the data
    $uri = thedatatank_build_request($resource, $package);
    $data = thedatatank_perform_request($uri);
    $data = json_decode($data);
    
    //Get the field to filter on
    $field = explode('_', $this->field);
    $field = $field[1];
    
    //Get the options
    $options = array();
    foreach ($data->{$package} as $row) {
    	if (!in_array($row->{$field}, $options)) {
    		if (is_numeric($row->{$field})) {
    		  $options[$row->{$field}] = $row->{$field};
    		} else {
    			$options[] = $row->{$field};
    		}
    	}
    }
    
    $this->value_options = $options;
  }
}
