<?php

/**
 * Filter by resource name.
 *
 * @ingroup views_filter_handlers
 */
class thedatatank_views_handler_filter_resource_name extends views_handler_filter_in_operator {
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    $this->value_options = array();
    $resources = thedatatank_get_resources();
    
    foreach ($resources as $resource => $packages) {
      $this->value_options[$resource] = $resource;
    }
  }
}
