<?php

/**
 * Field handler to provide simple renderer that allows linking to a taxonomy
 * term.
 *
 * @ingroup views_field_handlers
 */
class thedatadank_views_handler_field_thedatatank extends views_handler_field {
  /**
   * Constructor to provide additional field to add.
   *
   * This constructer assumes the taxonomy_term_data table. If using another
   * table, we'll need to be more specific.
   */
  function construct() {
    parent::construct();
    $this->additional_fields['package_name'] = 'package_name';
    $this->additional_fields['resource_name'] = 'resource_name';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_datatank'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Provide link to TheDatatank option
   */
  function options_form(&$form, &$form_state) {
    $form['link_to_datatank'] = array(
      '#title' => t('Link this field to The DataTank page.'),
      '#description' => t("Default .about page will be displayed in a separate window."),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_datatank']),
    );

    parent::options_form($form, $form_state);
  }

  /**
   * Render whatever the data is as a link to the taxonomy.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($values) {
    $url = thedatatank_build_request($values->thedatatankresources_resource_name, $values->thedatatankresources_package_name);
    $data = l($values->thedatatankresources_package_name, $url, array('attributes' => array('target' => '_blank')));
    return $data;
  }

  function render($values) {
  	if (strpos($this->field_alias, 'link_') === FALSE && $this->field_alias != 'thedatatankresources_documentation') {
      $value = $this->get_value($values);

      if ($this->options['link_to_datatank']) {
        return $this->render_link($values);	
      } else {
        return $value;
      }
  	} elseif ($this->field_alias == 'thedatatankresources_documentation') {
  		$documentation = thedatatank_get_documentation($values->thedatatankresources_resource_name, $values->thedatatankresources_package_name);
  		return $documentation;
    } else {
      $allowed_formatters = array_filter(variable_get('thedatatank_export_formatters', array()));
      if (empty($allowed_formatters)) $allowed_formatters = thedatatank_get_formatters();
      
      foreach ($allowed_formatters as $formatter) {
      	$formatter = strtolower($formatter);
      	if (strpos($this->field_alias, 'link_' . $formatter) !== FALSE) {
      		$resource = $values->thedatatankresources_resource_name;
      		$package = $values->thedatatankresources_package_name;
      		
      		$url = thedatatank_build_request($resource, $package) . '.' . $formatter;
      		return l(strtoupper($formatter), $url, array('attributes' => array('target' => '_blank')));
      	}
      }
    }
  }
}
