<?php

/**
 * Field handler to provide simple renderer that allows linking to a taxonomy
 * term.
 *
 * @ingroup views_field_handlers
 */
class thedatatank_views_handler_dynamic_field extends views_handler_field {
  /**
   * Constructor to provide additional field to add.
   *
   * This constructer assumes the taxonomy_term_data table. If using another
   * table, we'll need to be more specific.
   */
  function construct() {
    parent::construct();
    $this->additional_fields['field_rownumber'] = 'field_rownumber';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_datatank'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Provide link to TheDatatank option
   */
  function options_form(&$form, &$form_state) {
    $form['link_to_datatank'] = array(
      '#title' => t('Link this field to The DataTank page.'),
      '#description' => t("Default .about page will be displayed in a separate window."),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_datatank']),
    );

    parent::options_form($form, $form_state);
  }

  /**
   * Render whatever the data is as a link to the taxonomy.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($values) {
    /*$url = thedatatank_build_request($values->thedatatankresources_resource_name, $values->thedatatankresources_package_name);
    $data = l($values->thedatatankresources_package_name, $url, array('attributes' => array('target' => '_blank')));
    return $data;*/
  }

  function render($values) {
  	$field = explode('_', $this->field_alias);
  	unset($field[0]);
  	$field = implode('_', $field);
  	$value = $values[$field];
  	
  	//TODO use get_value;
  	//$value = $this->get_value($values, $field);
  	return $value;
  }
}
