<?php

/**
 * @file
 * Integrate the DataTank with Drupal
 * Admin interface
 */

/**
 * Admin settings form
 */
function thedatatank_admin_settings() {
	$form = array();
	
	$form['thedatatank_url'] = array(
	 '#type' => 'textfield',
	 '#title' => t('The DataTank URL'),
	 '#description' => t('URL where The DataTank installation can be found.'),
	 '#default_value' => variable_get('thedatatank_url', ''),
	);
	
	$form['thedatatank_api_user'] = array(
	 '#type' => 'textfield',
	 '#title' => t('API user'),
	 '#description' => t('The username of your API user on The DataTank installation.'),
	 '#default_value' => variable_get('thedatatank_api_user', ''),
	);
	
	$form['thedatatank_api_password'] = array(
	 '#type' => 'textfield',
	 '#title' => t('API password'),
	 '#description' => t('The password for the user above.'),
	 '#default_value' => variable_get('thedatatank_api_password', ''),
	);
	
	$form['thedatatank_phpexcel_installed'] = array(
	 '#type' => 'checkbox',
	 '#title' => t('PHPExcel installed?'),
	 '#description' => t('We need to know if PHPExcel is installed on The DataTank instance so we can accept XLS files or not.'),
	 '#default_value' => variable_get('thedatatank_phpexcel_installed', FALSE),
	);
	
	$form['thedatatank_exclude_tdt'] = array(
	 '#type' => 'checkbox',
	 '#title' => t('Exclude TDT Resources'),
	 '#description' => t('Exclude the default The DataTank resources (TDTInfo, TDTAdmin, TDTStatus) from being displayed in the interfaces.'),
	 '#default_value' => variable_get('thedatatank_exclude_tdt', FALSE),
	);
	
	$form['thedatatank_import_types'] = array(
	 '#type' => 'checkboxes',
	 '#title' => t('Allowed file types'),
	 '#description' => t('Restrict the list of file types to import.'),
	 '#options' => thedatatank_get_filetypes(),
	 '#default_value' => variable_get('thedatatank_import_types', array()),
	);
	
	$form['thedatatank_export_formatters'] = array(
	 '#type' => 'checkboxes',
	 '#title' => t('Allowed formatters'),
	 '#description' => t('Restrict the list of export formatters.'),
	 '#options' => thedatatank_get_formatters(),
	 '#default_value' => variable_get('thedatatank_export_formatters', array()),
	);
	
	return system_settings_form($form);
}