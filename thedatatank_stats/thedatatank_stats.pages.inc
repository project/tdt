<?php

/**
 * @file
 * Provide statistics about The DataTank usage
 */

/**
 * Overview page with general statistics on The DataTank
 */
function thedatatank_stats_overview() {
	$output = array();
	
	//Different display modes
	$items = array();
	$items[] = l(t('Columns'), $_GET['q'], array('query' => array('style' => 'column')));
	$items[] = l(t('Bars'), $_GET['q'], array('query' => array('style' => 'bar')));
	$items[] = l(t('Pie'), $_GET['q'], array('query' => array('style' => 'pie')));
	
	//Output type
	$style = 'column';
	$height = 200;
	
	if (isset($_GET['style'])) {
		$style = check_plain($_GET['style']);
		
		switch ($style) {
			case 'bar':
			case 'pie':
				$height = 400;
				break;
		}
	}
	
	$output[] = theme('item_list', array('items' => $items));
	
	$year = date('Y');
	$month = date('m');
	
	$datatank = variable_get('thedatatank_url', '');
	
	$resource = 'total';
	$package = 'total';
	$arg3 = arg(3);
	$arg4 = arg(4);
	
	if (!empty($arg3) && empty($arg4)) {
		$resource = check_plain($arg3);
		$output[] = '<h1>' . t('Statistics for resource @resource', array('@resource' => $resource)) . '</h1>';
	}
	
  if (!empty($arg4)) {
  	$resouce = check_plain($arg3);
  	$package = check_plain($arg4);
    $output[] = '<h1>' . t('Statistics for package @package', array('@package' => $package)) . '</h1>';
  }
  
	$output[] = '<h2>' . t('Total number of requests per day of current month') . '</h2>';
	$output[] = '<iframe src="' . $datatank . '/TDTStats/Day/' . $resource . '/' . $package . '/' . $year . '/' . $month . '/all.' . $style . '?category=day&value=requests" width="100%" height="' . $height . '"></iframe>';
	
  $output[] = '<h2>' . t('Total number of requests per month of current year') . '</h2>';
  $output[] = '<iframe src="' . $datatank . '/TDTStats/Month/' . $resource . '/' . $package . '/' . $year . '/all.' . $style . '?category=month&value=requests" width="100%" height="' . $height . '"></iframe>';
  
  $output[] = '<h2>' . t('Total number of request per year') . '</h2>';
  $output[] = '<iframe src="' . $datatank . '/TDTStats/Year/' . $resource . '/' . $package . '/all.' . $style . '?category=year&value=requests" width="100%" height="' . $height. '"></iframe>';
  
	return implode("\n", $output);
}