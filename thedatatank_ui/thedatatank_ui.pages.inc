<?php

/**
 * @file
 * User Interface for The DataTank API
 * Pages
 */

/**
 * Overview pages to start
 */
function thedatatank_ui_overview() {
	//Available resources
	$resources = thedatatank_get_resources();
	$output = '';
	
	foreach ($resources as $resource => $detail) {
		//Title
	 	$output .= sprintf('<h2>%s</h2>', $resource);
	 	
	 	//Table below
	 	$rows = array();
	 	$headers = array(
	 	 t('Package'),
	 	 t('Description'), 
	 	 ''
	 	);
	 	
	 	if (!in_array($resource, array('TDTInfo', 'TDTAdmin', 'TDTStats'))) {
	 		$headers[] = '';
	 	}
	   
	 	$packages = array_keys((array)$detail);
	 	
	 	foreach ($packages as $package) {
	 		$row = array(
	 		  $package,
	 		  (isset($detail->{$package}->documentation) ? $detail->{$package}->documentation : $detail->{$package}->doc),
	 		  l(t('View DataTank'), thedatatank_build_request($resource, $package), array('attributes' => array('target' => '_blank'))),
	 		);
	 		
	 		if (!in_array($resource, array('TDTInfo', 'TDTAdmin', 'TDTStats'))) {
	 			$row[] = l(t('Delete package'), 'admin/structure/thedatatank/delete', array('query' => array('resource' => $resource, 'package' => $package)));
	 		}
	 		
	 		$rows[] = $row;
	 	}

	 	$output .= theme('table', array(
	 	 'header' => $headers,
	 	 'rows' => $rows,
	 	));
	}
	
	//Available builds
	$builds = _thedatatank_ui_get_builds();
	$rows = array();
  $headers = array(t('Name'), t('Resource'), t('Package'), t('Display'));
	
	foreach ($builds as $bid => $build) {
		$row = array(
		  $build['buildname'],
		  $build['resource'],
		  $build['package'],
		  ucfirst($build['blockpage']),
		);
		
		$rows[] = $row;
	}
	
	if (!empty($rows)) {
		$output .= sprintf('<h2>%s</h2>', t('Builds'));
		$output .= theme('table', array(
		  'header' => $headers,
		  'rows' => $rows,
		));
	}
	
	return $output;
}

/**
 * Form to build queries on The DataTank
 */
function thedatatank_ui_builder($form, &$form_state) {
	$form = array();
	
	$form['buildname'] = array(
	 '#type' => 'textfield',
	 '#title' => t('Name'),
	 '#description' => t('Provide a comprehensive name for your build.'),
	);
	
	$form['resource'] = array(
	 '#type' => 'select',
	 '#title' => t('Resource'),
	 '#description' => t('Select the resource you want to query.'),
	 '#options' => thedatatank_get_resources_as_options(),
	 '#required' => TRUE,
	 '#ajax' => array(
	   'callback' => 'thedatatank_ui_refresh_packages',
	   'wrapper' => 'package_wrapper',
	 ),
	);
	
	(isset($form_state['values']['resource']) ? $packages = thedatatank_get_packages($form_state['values']['resource']) : $packages = array());
	
	$form['package'] = array(
	 '#type' => 'select',
	 '#title' => t('Package'),
	 '#options' => $packages,
	 '#prefix' => '<div id="package_wrapper">',  
	 '#suffix' => '</div>',
	 '#required' => TRUE,
	 '#ajax' => array(
	   'event' => 'change',
	   'callback' => 'thedatatank_ui_import_get_params',
	   'wrapper' => 'parameters_wrapper',
	   'effect' => 'fade',
	 ),
	);
	
  $form['params'] = array(
    '#type' => 'fieldset',
    '#title' => t('Parameters and visualisation'),
    '#prefix' => '<div id="parameters_wrapper" style="display: none;">',
    '#suffix' => '</div>',
  );

  if (isset($form_state['values']['package'])) {
  	$variables = array(
  	 $form_state['values']['resource'],
  	 $form_state['values']['package'],
  	);
  	
    $params = thedatatank_get_params($variables, $form_state['values']['package']);
    
    //These fields are in the Drupal form
    unset($params['params']->uri);
    unset($params['params']->documentation);
    
    //Create 2 fieldsets for the params.
    $form['params']['required'] = array(
      '#type' => 'fieldset',
      '#title' => t('Required parameters'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    
    $form['params']['optional'] = array(
      '#type' => 'fieldset',
      '#title' => t('Optional parameters'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    
    foreach ($params['params'] as $param => $description) {
      //Detect the fieldset to put the field
      $form['params'][(in_array($param, $params['required']) ? 'required' : 'optional')]['param_' . $param] = array(
       '#type' => 'textfield',
       '#title' => $param,
       '#description' => t($description),
       '#required' => (in_array($param, $params['required']) ? TRUE : FALSE),
       '#default_value' => (isset($default_values[$param]) ? $default_values[$param] : ''),
       '#attributes' => (isset($default_values[$param]) ? array('readonly' => 'readonly') : array()),
      );
    }
    
    //If the fieldsets for required and optional params don't have any children, unset them.
    if (count($form['params']['required']) == 4) unset($form['params']['required']);
    if (count($form['params']['optional']) == 4) unset($form['params']['optional']);
    
    if (count($form['params']) == 4) {
    	$form['params']['parameters'] = array(
    	 '#markup' => t('There are no parameters available for this package.'),
    	);
    }
    
    $form['params']['visualization'] = array(
      '#type' => 'fieldset',
      '#title' => t('Visualisation'),
      '#description' => t('Display settings on how the data will be displayed in Drupal and where.')
    );
    
    $form['params']['visualization']['blockpage'] = array(
      '#type' => 'radios',
      '#title' => t('Block or page'),
      '#description' => t('Provide a block or a page for this data.'),
      '#options' => array(
        'block' => t('Provide a block for this data.'),
        'page' => t('Provide a page for this data.'),
      ),
      '#ajax' => array(
        'event' => 'change',
        'callback' => 'thedatatank_ui_import_get_pageinfo',
        'wrapper' => 'page_wrapper',
        'effect' => 'fade',
      ),
      '#required' => TRUE,
    );
    
    $form['params']['visualization']['page_wrapper'] = array(
      '#type' => 'fieldset',
      '#title' => t('Page information'),
      '#prefix' => '<div id="page_wrapper" style="display: none;">',
      '#suffix' => '</div>',
    );
    
    if (isset($form_state['values']['blockpage']) && $form_state['values']['blockpage'] == 'page') {
    	$form['params']['visualization']['page_wrapper']['page_title'] = array(
    	 '#type' => 'textfield',
    	 '#title' => t('Page title'),
    	 '#description' => t('Enter the title for this page'),
    	 '#required' => TRUE,
    	);
    	
    	$form['params']['visualization']['page_wrapper']['page_url'] = array(
    	 '#type' => 'textfield',
    	 '#title' => t('Path'),
    	 '#description' => t('Enter the patch for this page'),
    	 '#required' => TRUE,
    	);
    } else {
    	$form['params']['visualization']['page_wrapper']['#attributes'] = array('style' => 'display: none;');
    }
    
    $form['params']['visualisation']['display'] = array(
      '#type' => 'select',
      '#title' => t('Display'),
      '#description' => t('Theme the data yourself or choose a visualisation format from The Datatank'),
      '#options' => _thedatatank_ui_get_visualizations(),
      '#required' => TRUE,
    );
    
    $form['params']['visualisation']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Create view'),
    );
  }
  
	return $form;
}

/**
 * Ajax callback to refresh the packages in the selected resource
 */
function thedatatank_ui_refresh_packages($form, &$form_state) {
	return $form['package'];
}

/**
 * Ajax callback to get extra page information
 */
function thedatatank_ui_import_get_pageinfo($form, &$form_state) {
	return $form['params']['visualization']['page_wrapper'];
}

//TODO Validation of the builder form

/**
 * Submit function for the builder form
 */
function thedatatank_ui_builder_submit($form, &$form_state) {
	//Unset all values that have nothing to do with the actual builder data
	foreach ($form_state['values'] as $key => $value) {
		if (strpos($key, 'form_') !== FALSE || in_array($key, array('op', 'submit'))) unset($form_state['values'][$key]);
	}
	
	//Save the build
	_thedatatank_ui_save_build($form_state['values']);
	
	//Goto overview page
	drupal_goto('admin/structure/thedatatank/overview');
}

/**
 * Form to import data into The DataTank
 */
function thedatatank_ui_import($form, &$form_state) {
	//TODO improve usability: add resource and package selectbox
	$form = array();
	
	$form['resource'] = array(
   '#type' => 'textfield',
   '#title' => t('Resource'),
   '#description' => t('The resource to put on'),
   '#required' => TRUE,
  );
  
  $form['package'] = array(
   '#type' => 'textfield',
   '#title' => t('Package'),
   '#description' => t('The package to put on'),
   '#required' => TRUE,
  );
  
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('Will be used as documentation in The DataTank'),
    '#required' => TRUE,
  );
	
	$form['file'] = array(
	 '#type' => 'file',
	 '#title' => t('File to import'),
	);
	
	//Filetypes
	$tdt_filetypes = thedatatank_get_filetypes();
	$drup_filetypes = variable_get('thedatatank_import_types', array());
	$drup_filetypes = array_filter($drup_filetypes);
	
	if (!empty($drup_filetypes)) {
		foreach ($tdt_filetypes as $key => $value) {
			if (!in_array($key, $drup_filetypes)) {
				unset($tdt_filetypes[$key]);
			}
		}
	}
	
	$form['filetype'] = array(
	 '#type' => 'select',
	 '#title' => t('File type'),
	 '#description' => t('Select the filetype of the file to import'),
	 '#required' => TRUE,
	 '#options' => $tdt_filetypes,
	 '#ajax' => array(
	   'event' => 'change',
	   'callback' => 'thedatatank_ui_import_get_params',
	   'wrapper' => 'thedatatank_ui_import_params',
	   'effect' => 'fade',
	 ),
	);
	
	$form['params'] = array(
	 '#type' => 'fieldset',
	 '#title' => t('Parameters'),
	 '#prefix' => '<div id="thedatatank_ui_import_params" style="display: none;">',
	 '#suffix' => '</div>',
	);

	if (isset($form_state['values']['filetype'])) {
		$params = _thedatatank_ui_filetype_params($form_state['values']['filetype']);
		
		//These fields are in the Drupal form
		unset($params['params']->uri);
		unset($params['params']->documentation);
		
		//Those values are
		$default_values = array(
		  'resource_type' => 'generic',
		  'generic_type' => $form_state['values']['filetype'],
		);
		
		//Create 2 fieldsets for the params.
		$form['params']['required'] = array(
		  '#type' => 'fieldset',
		  '#title' => t('Required parameters'),
		  '#collapsible' => FALSE,
		);
		
		$form['params']['optional'] = array(
		  '#type' => 'fieldset',
		  '#title' => t('Optional parameters'),
		  '#collapsible' => TRUE,
		  '#collapsed' => TRUE,
		);
		
		foreach ($params['params'] as $param => $description) {
			//Detect the fieldset to put the field
			$form['params'][(in_array($param, $params['required']) ? 'required' : 'optional')]['param_' . $param] = array(
			 '#type' => 'textfield',
			 '#title' => $param,
			 '#description' => t($description),
			 '#required' => (in_array($param, $params['required']) ? TRUE : FALSE),
			 '#default_value' => (isset($default_values[$param]) ? $default_values[$param] : ''),
			 '#attributes' => (isset($default_values[$param]) ? array('readonly' => 'readonly') : array()),
			);
		}
	}
	
	$form['submit'] = array(
	 '#type' => 'submit',
	 '#value' => t('Import'),
  );
	
	return $form;
}

/**
 * Ajax callback for the parameters part
 */
function thedatatank_ui_import_get_params($form, &$form_state) {
	return $form['params'];
}

/**
 * File upload for the UI import form
 */
function thedatatank_ui_import_validate($form, &$form_state) {
	//TODO better directory structure in files folder
  $file = file_save_upload('file', array(
    'file_validate_extensions' => array('csv xml'), // Validate extensions.
  ));

  if ($file) {
    if ($file = file_move($file, 'public://thedatatank')) {
      $form_state['storage']['file'] = $file;
    } else {
      form_set_error('file', t('Failed to write the uploaded file the site\'s file folder.'));
    }
  } else {
    form_set_error('file', t('No file was uploaded.'));
  }
}

/**
 * Submit function for the UI import form
 */
function thedatatank_ui_import_submit($form, &$form_state) {
	$file = $form_state['storage']['file'];
	$file = file_create_url($file->uri);
	$resource = $form_state['values']['resource'];
	$package = $form_state['values']['package'];
	$documentation = $form_state['values']['description'];
	$params = array();
	
	//Location to put
	$location = array(
	 'resource' => $resource,
	 'package' => $package,
	);
	
	//Build the param array
	$params = array();
	
	foreach ($form_state['values'] as $field => $value) {
		if (strpos($field, 'param_') !== FALSE && !empty($value)) {
			$param = str_replace('param_', '', $field);
			$params[$param] = $value;
		}
	}
	
	//Add parameters also needed, but coming from Drupal fields
	$params['uri'] = $file;
	$params['documentation'] = $documentation;
	
	//Put the file
	$response = thedatatank_put_data($location, $params);
	
  if (empty($response)) {
    //TODO HTTP 401 also gives empty response, validate sequence
    drupal_set_message(t('Import succesfull. You will found your newly created dataset in the overview page.'));
  } else {
    drupal_set_message(t('Something went wrong during import. View the watchdog table for exact error message.'));
  }
  
  //TODO We also  have to clear cached data in Drupal because this file can be an update of the current data
  //TODO We better pre-cache the data also
}

/**
 * Delete form
 */
function thedatatank_ui_delete() {
	$form = array();
	
	if (isset($_GET['resource'])) {
		$form['resource'] = array(
		  '#type' => 'hidden',
		  '#value' => check_plain($_GET['resource']),
		);
		
		$question = t('Are you sure you want to delete this resource?');
	}
	
	if (isset($_GET['package'])) {
		$form['package'] = array(
      '#type' => 'hidden',
      '#value' => check_plain($_GET['package']),
    );
    
    $question = t('Are you sure you want to delete this package?');
	}

	return confirm_form($form,
	 $question,
	 'admin/structure/thedatatank/overview',
	 t('This action cannot be undone.'),
	 t('Delete'),
	 t('Cancel')
	);
}

/**
 * Submit function after conform for the delete form
 */
function thedatatank_ui_delete_submit($form, &$form_state) {
	$resource = $form_state['values']['resource'];
	$package = '';
	
	if (isset($form_state['values']['package'])) {
		$package = $form_state['values']['package'];
	}

	$response = thedatatank_delete_data($resource, $package);
	
  if (empty($response)) {
    //TODO HTTP 401 also gives empty response, validate sequence
    drupal_set_message(t('Package deleted.'));
  } else {
  	//TODO Write watchdog message
    drupal_set_message(t('Something went wrong. View the watchdog table for exact error message.'));
  }
	
	drupal_goto('admin/structure/thedatatank/overview');
}